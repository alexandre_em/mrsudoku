(ns mrsudoku.solver
  (:use midje.sweet)
  (:require [mrsudoku.grid :as g]
            [mrsudoku.engine :as e]))

(def ^:private sudoku-grid (var-get #'g/sudoku-grid))
(def grid1 (g/gen-grid (g/vec-grid "3.txt")))

(defn check-cells
  "Verifie si il reste des cellules vide dans la grille"
  [grid]
  (g/reduce-grid (fn [acc cx cy cell]
                   (if (nil? (g/cell-value cell))
                     true
                     (or false acc)))
                 false grid))

(fact
 (check-cells sudoku-grid) => true)

(defn possible-value
  "Retourne les valeurs possible a la cellule (cx cy) du block `b`"
  [grid cx cy b]
  (let [val (into #{} (range 1 10))
        row (clojure.set/difference val (e/values (g/row grid cy)))
        col (clojure.set/difference row (e/values (g/col grid cx)))
        block (clojure.set/difference col (e/values (g/block grid b)))]
    block))

(fact (possible-value sudoku-grid 8 8 9) => #{3})

(fact (possible-value sudoku-grid 3 9 7) => #{1 4 2 5 3})

(fact (possible-value sudoku-grid 5 9 8) => #{3 5})

(defn build-cells
  [grid row ligne x j]
  (loop [res [], bl (first row), cx (+ (* 3 x) 1), cy (+ (* 3 ligne) 1)]
    (if (seq bl)
      (recur (conj res (if (nil? (g/cell-value (g/cell grid cx cy)))
                          {:x cx :y cy :block j :value (possible-value grid cx cy j)}
                          {}))
             (rest bl) 
             (if (= cx (+ (* 3 x) 3))
               (+ (* 3 x) 1)
               (inc cx)) 
             (if (= cx (+ (* 3 x) 3))
               (inc cy)
               cy))
      res)))

(defn build-blocks
  "Construit les block d'une rangee"
  [grid gr ligne]
  (loop [res [], row (first gr), j (inc (* ligne 3)), x 0] ;; j: numero block
    (if (seq row)
      (recur (conj res (build-cells grid row ligne x j)) (rest row) (inc j) (inc x))
      res)))

(fact (ffirst (build-blocks sudoku-grid sudoku-grid 1))
       => {})

(fact (first (first (build-blocks sudoku-grid (rest sudoku-grid) 2)))
       => {:x 1, :y 7, :block 7, :value #{1 3 9}})

(defn possible-grid
  "Construit une grille des valeurs possibles `pg`"
  [grid]
  (loop [res [], gr grid, i 0] ;; i: numero de range
    (if (seq gr)
      (recur (conj res (build-blocks grid gr i)) (rest gr) (inc i))
      res)))

(fact (g/cell (possible-grid sudoku-grid) 8 8) =>
      {:x 8, :y 8, :block 9, :value #{3}})

(fact (g/cell (possible-grid sudoku-grid) 5 9) => {})

(defn to-set-uniq
  "Met a jour les cellules a set dont le nombre de valeur possible pour une cellule est de 1"
  [grid pg]
  (loop [gr grid i 1 j 1]
    (if (< i 10)
      (let [values (get (g/cell pg i j) :value)
            g (if (= 1 (count values))
                (g/change-cell gr i j (g/mk-cell :solved  (first values)))
                gr)]
        (if (< j 9)
          (recur g i (inc j))
          (recur g (inc i) 1)))
      gr)))

(fact (let [grid sudoku-grid
            pg (g/change-cell (possible-grid grid) 3 1 {:x 3 :y 1 :block 1 :value #{9}})]
        (g/cell (to-set-uniq grid pg) 3 1)) => {:status :solved, :value 9})


(defn filtre-cells
  "Met a jour les cellules `opt` a set dont le nombre de valeur possible pour une cellule est de 1.
   valeur possible pour `opt` : g/row, g/col, g/block"
  [grid opt]
  {:pre [(or (= opt g/col) (= opt g/row) (= opt g/block))]}
  (loop [gr grid i 1]
    (if (< i 10)
      (recur (loop [gr gr, values (opt (possible-grid gr) i), j 0]
               (if (< j 9)
                 (let [s (reduce (fn [acc cells]
                                   (clojure.set/union acc (get cells :value))) #{} (concat (take j values) (drop (inc j) values)))
                       cell (nth values j)
                       uniq? (clojure.set/difference (get cell :value) s)]
                   (if (= (count uniq?) 1)
                     (recur (g/change-cell gr (get cell :x) (get cell :y) (g/mk-cell :solved (first uniq?)))
                            values (inc j))
                     (recur gr values (inc j))))
                 gr)) (inc i))
      gr)))

(fact (g/block (filtre-cells sudoku-grid g/row) 6)
      =>
      [{:status :empty}
       {:status :empty}
       {:status :init, :value 3}
       {:status :solved, :value 7}
       {:status :empty}
       {:status :init, :value 1}
       {:status :solved, :value 8}
       {:status :empty}
       {:status :init, :value 6}])

(defn filtre-lock
  "Renvoie un ensemble de valeur possible qui ne peuvent etre que dans le `block` a la row/col(`opt`) j"
  [block cells j opt]
  {:pre [(or (= opt g/block-row) (= opt g/block-col))]}
  (loop [k (inc j) s #{}]
    (if (< k 4)
      (recur (inc k) (clojure.set/union s (reduce (fn [acc bl]
                                                    (if (seq bl)
                                                      (clojure.set/union acc (get bl :value))
                                                      acc)) #{} (opt block k))))
      (let [cmp (reduce (fn [acc bl]
                          (if (seq bl)
                            (clojure.set/union acc (get bl :value))
                            acc)) #{} cells)]
        (if (seq s)
          (clojure.set/difference cmp s)
          #{})))))

(fact (let [bl (g/block (possible-grid grid1) 8)
            br (g/block-row bl 1)]
        (filtre-lock bl br 1 g/block-row)) => #{7})

(defn locked
  "Supprime les valeurs obtenues grace au `filtre-lock` des cellules `opt` n'appartenant pas au block courant `n`"
  [pgrid cells s n opt]
  {:pre [(or (= opt g/row) (= opt g/col))]}
  (loop [pgrid pgrid
         v (opt pgrid (get (loop [cells cells]
                             (if (and (seq cells) (empty? (first cells)))
                               (recur (rest cells))
                               (first cells)))
                           (if (= opt g/row)
                             :y
                             :x)))]
    (if (seq v)
      (let [cell (first v)]
        (if (and (seq cell) (not= n (get cell :block)))
          (recur (g/change-cell pgrid (get cell :x) (get cell :y) (assoc cell :value (clojure.set/difference (get cell :value) s))) (rest v))
          (recur pgrid (rest v))))
      pgrid)))



(fact "Valeurs possible apres application de la fonction: suppression de 7 pour `block` different de 8"
      (let [bl (g/block (possible-grid grid1) 8)
            br (g/block-row bl 1)]
        (g/row (locked (possible-grid grid1) br (filtre-lock bl br 1 g/block-row) 8 g/row) 7))
      =>
      [{}
       {}
       {}
       {:x 4, :y 7, :block 8, :value #{1 6 2}}
       {:x 5, :y 7, :block 8, :value #{7 1 6 2 9}}
       {:x 6, :y 7, :block 8, :value #{7 1 6 9}}
       {}
       {:x 8, :y 7, :block 9, :value #{1 6}}
       {:x 9, :y 7, :block 9, :value #{1 6 2 9}}])

(defn lock-grid
  "Parcours tous les `block` de la `possible-grid` et les filtre selon `opt` `opt2`"
  [grid opt opt2]
  {:pre [(or (= opt g/block-row) (= opt g/block-col))
         (or (= opt2 g/row) (= opt2 g/col))]}
  (loop [grid grid i 1]
    (if (< i 10)
      (recur (loop [grid grid pg (possible-grid grid) bl (g/block pg i) j 1 br (opt bl j)]
                                        (if (< j 4)
                                          (let [inter (filtre-lock bl br j opt)]
                                            (if (seq inter)
                                              (let [pg (locked pg br inter i opt2)
                                                    bl (g/block pg i)]
                                                (recur (to-set-uniq grid pg) pg bl (inc j) (opt bl j)))
                                              (let [pg (possible-grid grid)
                                                    bl (g/block pg i)]
                                                (recur (to-set-uniq grid pg) pg bl (inc j) (opt bl j)))))
                                          grid)) (inc i))
      grid)))

(fact (g/row (lock-grid sudoku-grid g/block-row g/row) 7)
      => [{:status :solved, :value 9}
          {:status :init, :value 6}
          {:status :solved, :value 1}
          {:status :solved, :value 5}
          {:status :solved, :value 3}
          {:status :solved, :value 7}
          {:status :init, :value 2}
          {:status :init, :value 8}
          {:status :solved, :value 4}])


(defn solve
  "Met des valeurs dans les cellules jusqu'a qu'il n'y ait plus de cellules vide"
  [grid]
  (loop [gr grid, old nil i 0]
    (if (and (check-cells gr) (< i 100))
      (if (= gr old)
        (recur (lock-grid (lock-grid gr g/block-row g/row) g/block-col g/col) gr (inc i))
        (recur (filtre-cells (filtre-cells (filtre-cells (to-set-uniq gr (possible-grid gr)) g/block) g/col) g/row) gr (inc i)))
      gr)))

(fact "Aucun conflict detecte"
 (e/grid-conflicts (solve sudoku-grid)) => {})

(fact "Aucune cellule vide"
 (check-cells (solve sudoku-grid)) => false)
